$(function(){
	var $jsTabsWr = $('.js-tabs-wr ');
	if(!$jsTabsWr.length) return;

	var $jsTabs = $jsTabsWr.find('.js-tabs a'),
		$jsTabCont = $jsTabsWr.find('.js-tab-cont > div');

    $jsTabs.click(function(){
	    $jsTabCont.addClass('hide');
	    $jsTabs.parent().removeClass('active');
	    
	    var id = $(this).attr('href');
	    $(id).removeClass('hide');
	    $(this).parent().addClass('active');
	    return false;
  });
});

$(function() {
	$(".mask_phone_section").inputmask({ 
		mask: "+7 (A99) 999-9999",
		definitions:{ 
		'A': { 
			validator: "[0-69]", 
			cardinality: 1 
			} 
		},
		"onincomplete": function(){ 
		$(this).addClass('error'); 
		}, 
		"oncomplete": function(){ 
		$(this).removeClass('error').addClass('ok'); 
		}
	});
});

$(function() {

	$('.js-clients-slider').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		prevArrow: '<button type="button" class="slick-prev"><span class="icon-left-arrow"></span></button>',
		nextArrow: '<button type="button" class="slick-next"><span class="icon-right-arrow"></span></button>',
		responsive: [
		{
			breakpoint: 991,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
				dots: true
			}
		},
		{
			breakpoint: 767,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
				infinite: true,
				dots: true
			}
		},
		{
			breakpoint: 479,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		}
		]
	});
});

$(function() {

	$('.js-gallery').each(function() { // the containers for all your galleries
	    $(this).magnificPopup({
	        delegate: 'a', // the selector for gallery item
	        type: 'image',
	        removalDelay: 300,
	        mainClass: 'mfp-fade',
	        closeOnBgClick: false,
	        gallery: {
	          	enabled:true,
	            tPrev: '', // title for left button
				tNext: '', // title for right button
				arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"><span class="icon-%dir%-arrow"></span></button>',
				tCounter: '<span class="mfp-counter">%curr% из %total%</span>' // markup of counter
	        },
	        image: {
	            titleSrc: 'title' 
	            // this tells the script which attribute has your caption
	        }
	    });
	});

});

$(function() {

 	$('.js-popup-youtube').magnificPopup({
	    disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false,
		iframe: {
	        patterns: {
	            youtube: {
	                src: 'http://www.youtube.com/embed/%id%?autoplay=1&rel=0'
	            }
	        }
	    }
	});
});

$(function() {

	var $videoSlider = $('.js-video-slider');

	$videoSlider.slick({
		dots: false,
		infinite: false,
		speed: 300,
		prevArrow: '<button type="button" class="slick-prev"><span class="icon-transfer1"></span></button>',
		nextArrow: '<button type="button" class="slick-next"><span class="icon-transfer"></span></button>',
		appendArrows: $('.js-video-slider-nav')
	});

	$videoSlider.on('afterChange', function(event, slick, currentSlide){
	  $('.js-cur-of-slides').html($videoSlider.slick('slickCurrentSlide') + 1);
	});

	$('.js-count-of-slides').html($videoSlider.slick("getSlick").slideCount);

});


// $(function() {

	
// });

$(function() {

	$(".main-screen").on("click","a.btn--service", function (event) {
		//отменяем стандартную обработку нажатия по ссылке
		event.preventDefault();

		//забираем идентификатор бока с атрибута href
		var id  = $(this).attr('href'),
			//узнаем высоту от начала страницы до блока на который ссылается якорь
			top = $(id).offset().top;
		
		//анимируем переход на расстояние - top за 1500 мс
		$('body,html').animate({scrollTop: top}, 1500);
	});
});

$(function() {

	$('.js-popup-modal').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#username',
		showCloseBtn: true,
		removalDelay: 300,
	    mainClass: 'mfp-fade'
	});

	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	$('.js-popup-modal').click(function(){
		var btnName = $(this).data('name');
		$('#btnname').val(btnName);
	});

	$('.js-form').each(function(){
		// Объявляем переменные (форма и кнопка отправки)
		var form = $(this),
		    btn = form.find('.js-popup-btn');

		// Добавляем каждому проверяемому полю, указание что поле пустое
		form.find('.rfield').addClass('empty_field');

		// Функция проверки полей формы
		function checkInput(){
			form.find('.rfield').each(function(){
				if($(this).val() != ''){
					  // Если поле не пустое удаляем класс-указание
					$(this).removeClass('empty_field');
				} else {
					  // Если поле пустое добавляем класс-указание
					$(this).addClass('empty_field');
				}
			});
		}

		// Функция подсветки незаполненных полей
		function lightEmpty(){
			form.find('.empty_field').addClass('error');
			// Через полсекунды удаляем подсветку
			setTimeout(function(){
			form.find('.empty_field').removeClass('error');
			},500);
		}

		// Проверка в режиме реального времени
		setInterval(function(){
			// Запускаем функцию проверки полей на заполненность
			checkInput();
			// Считаем к-во незаполненных полей
			var sizeEmpty = form.find('.empty_field').size();
			// Вешаем условие-тригер на кнопку отправки формы
			if(sizeEmpty > 0){
			if(btn.hasClass('disabled')){
			  return false
			} else {
				btn.addClass('disabled')
			}
			} else {
				btn.removeClass('disabled')
			}
		}, 500);


		// Событие клика по кнопке отправить
		btn.click(function(){
		  if($(this).hasClass('disabled')){
		    // подсвечиваем незаполненные поля и форму не отправляем, если есть незаполненные поля
			lightEmpty();
			return false
		  } else {
				form.submit(function (){

					var data = form.serialize();

					$.ajax({ 
						// инициaлизируeм ajax зaпрoс
						type: 'POST', // oтпрaвляeм в POST фoрмaтe, мoжнo GET
						url: 'mail.php', // путь дo oбрaбoтчикa, у нaс oн лeжит в тoй жe пaпкe
						data: data, // дaнныe для oтпрaвки
						error: function(xhr, status, error) {
							console.log(xhr.responseText + '|\n' + status + '|\n' +error);
						},
						success: function(data){ // сoбытиe пoслe удaчнoгo oбрaщeния к сeрвeру и пoлучeния oтвeтa
							$.magnificPopup.open({
								showCloseBtn: false,
								removalDelay: 350,
	    						mainClass: 'mfp-fade',
								items: {
									src: '#after-modal'
								},
								type: 'inline'
								},
							0);
							form[0].reset();
						}
					});

					return false;
				});
			}
		});
	});

});


